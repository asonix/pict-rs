use std::{
    ffi::{OsStr, OsString},
    time::Duration,
};

use streem::IntoStreamer;
use tokio::io::AsyncReadExt;

use crate::{
    bytes_reader::BytesReader,
    details::Details,
    error::{Error, UploadError},
    formats::ProcessableFormat,
    future::{WithMetrics, WithTimeout},
    magick::{MagickError, MAGICK_CONFIGURE_PATH, MAGICK_TEMPORARY_PATH},
    process::Process,
    repo::Hash,
    state::State,
    store::Store,
};

pub(crate) async fn generate<S>(
    state: &State<S>,
    hash: Hash,
    original_details: &Details,
) -> Result<String, Error>
where
    S: Store + 'static,
{
    if state.config.server.danger_dummy_mode {
        return Ok(String::from("LGF5?xYk^6AAAAAAAAAAAAAAAAAA"));
    }

    let permit = crate::process_semaphore().acquire().await?;

    let blurhash = do_generate(state, hash, original_details)
        .with_timeout(Duration::from_secs(state.config.media.process_timeout * 4))
        .with_metrics(crate::init_metrics::GENERATE_BLURHASH)
        .await
        .map_err(|_| UploadError::ProcessTimeout)??;

    drop(permit);

    Ok(blurhash)
}

pub(crate) async fn do_generate<S>(
    state: &State<S>,
    hash: Hash,
    original_details: &Details,
) -> Result<String, Error>
where
    S: Store + 'static,
{
    let identifier = if original_details.is_video() {
        crate::generate::ensure_motion_identifier(state, hash, original_details).await?
    } else {
        state
            .repo
            .identifier(hash)
            .await?
            .ok_or(UploadError::MissingIdentifier)?
    };

    let input_details = crate::ensure_details_identifier(state, &identifier).await?;

    let stream = state.store.to_stream(&identifier, None, None).await?;

    match input_details.internal_format().image_rs_format() {
        // supported pure-rust image decoders
        Some(
            format @ image::ImageFormat::Gif
            | format @ image::ImageFormat::Jpeg
            | format @ image::ImageFormat::Png
            | format @ image::ImageFormat::WebP,
        ) => {
            let (tx, bytes_reader) = BytesReader::new(8);

            let blurhash_task = crate::sync::spawn_blocking("image-blurhash", move || {
                let raw_image = image::ImageReader::with_format(bytes_reader, format)
                    .decode()
                    .map_err(UploadError::Decode)?
                    .into_rgba8();

                let blurhash = blurhash_update::auto_encode(
                    blurhash_update::ImageBounds {
                        width: raw_image.width(),
                        height: raw_image.height(),
                    },
                    raw_image.as_raw(),
                );

                Ok(blurhash) as Result<_, UploadError>
            });

            let stream = std::pin::pin!(stream);
            let mut streamer = stream.into_streamer();

            while let Some(res) = streamer.next().await {
                tx.send(res).await.map_err(|_| UploadError::Canceled)?;
            }
            drop(tx);

            let blurhash = blurhash_task.await.map_err(|_| UploadError::Canceled)??;

            Ok(blurhash)
        }
        _ => {
            let blurhash = read_rgba_command(
                state,
                input_details
                    .internal_format()
                    .processable_format()
                    .expect("not a video"),
            )
            .await?
            .drive_with_stream(stream)
            .with_stdout(|mut stdout| async move {
                let mut encoder = blurhash_update::Encoder::auto(blurhash_update::ImageBounds {
                    width: input_details.width() as _,
                    height: input_details.height() as _,
                });

                let mut buf = [0u8; 1024 * 8];

                loop {
                    let n = stdout.read(&mut buf).await?;

                    if n == 0 {
                        break;
                    }

                    encoder.update(&buf[..n]);
                }

                Ok(encoder.finalize()) as std::io::Result<String>
            })
            .await??;

            Ok(blurhash)
        }
    }
}

async fn read_rgba_command<S>(
    state: &State<S>,
    input_format: ProcessableFormat,
) -> Result<Process, MagickError> {
    let temporary_path = state
        .tmp_dir
        .tmp_folder()
        .await
        .map_err(MagickError::CreateTemporaryDirectory)?;

    let mut input_arg = OsString::from(input_format.magick_format());
    input_arg.push(":-");
    if input_format.coalesce() {
        input_arg.push("[0]");
    }

    let args: [&OsStr; 3] = ["convert".as_ref(), &input_arg, "RGBA:-".as_ref()];

    let envs = [
        (MAGICK_TEMPORARY_PATH, temporary_path.as_os_str()),
        (MAGICK_CONFIGURE_PATH, state.policy_dir.as_os_str()),
    ];

    let process = Process::run("magick", &args, &envs, state.config.media.process_timeout)
        .await?
        .add_extras(temporary_path);

    Ok(process)
}
