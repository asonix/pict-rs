# pict-rs 0.5.17-pre.9

pict-rs is a simple image hosting microservice, designed to handle storing and retrieving images,
animations, and videos, as well as providing basic image processing functionality.

## Overview

pict-rs 0.5.17-pre.9 includes dependency updates, a fix for danger-dummy-mode, and performance
improvements for blurhashing.


## Upgrade Notes

There are no significant changes from 0.5.17-pre.8. Upgrading should be as simple as pulling a new
version of pict-rs.
